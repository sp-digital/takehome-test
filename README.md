# Take Home Tests

These are sets of Take Home Tests for potential candidates:

- [Designer](#designer)
- [Full Stack Engineer](#full-stack-engineer)
- [Frontend Engineer](#frontend-engineer)
- [iOS Engineer](#ios-engineer)
- [Android Engineer](#android-engineer)
- [IoT Engineer](#iot-engineer)

----

## Designer

### Background

In the far away future, each household in Singapore will be equipped with a central Smart Meter
that can measure the household's electricity usage in real-time.

The Smart Meter is also capable of breaking down the electricity usage by device/equipment.

### Your Task

Design a dashboard for an iPad app that would display critical and relevant information to home owners,
based on data collected by the Smart Meter, for them to better understand their household's electricity consumption.

Please provide at least 2 screens (vertical and horizontal) of the main view,
and as many screens showing the other functionalities of the app.

### Time

7 days from XXXX. Please feel free to submit your work any time, before the deadline.

Please timebox yourself to a maximum of 8 hours for this activity,
and provide a short writeup (< 500 words) of your design process.

### Follow Up

In the event that you are selected for the next round of interview (onsite chat),
please be expected to discuss your work further with us during the interview.

----

## Full Stack Engineer

### Background

For any application with a need to build its own social network, "Friends Management" is a common requirement
which usually starts off simple but can grow in complexity depending on the application's use case.

Usually, applications would start with features like "Friend", "Unfriend", "Block", "Receive Updates" etc.

### Your Task

Develop an API server that does simple "Friend Management" based on the User Stories below.

You are required to:

- Deploy an instance of the API server on the public cloud or provide a 1-step command to run your API server locally, e.g. using a Makefile or Docker Compose) for us to test run the APIs
- Write sufficient documentation for the APIs and explain your technical choices

#### User Stories

**1. As a user, I need an API to create a friend connection between two email addresses.**

The API should receive the following JSON request:

``` json
{
  "friends":
    [
      "andy@example.com",
      "john@example.com"
    ]
}
```

The API should return the following JSON response on success:

``` json
{
  "success": true
}
```

Please propose JSON responses for any errors that might occur.

**2. As a user, I need an API to retrieve the friends list for an email address.**

The API should receive the following JSON request:

``` json
{
  "email": "andy@example.com"
}
```

The API should return the following JSON response on success:

``` json
{
  "success": true,
  "friends" :
    [
      "john@example.com"
    ],
  "count" : 1
}
```

Please propose JSON responses for any errors that might occur.

**3. As a user, I need an API to retrieve the common friends list between two email addresses.**

The API should receive the following JSON request:

``` json
{
  "friends":
    [
      "andy@example.com",
      "john@example.com"
    ]
}
```

The API should return the following JSON response on success:

``` json
{
  "success": true,
  "friends" :
    [
      "common@example.com"
    ],
  "count" : 1
}
```

Please propose JSON responses for any errors that might occur.

**4. As a user, I need an API to subscribe to updates from an email address.**

Please note that "subscribing to updates" is NOT equivalent to "adding a friend connection".

The API should receive the following JSON request:

``` json
{
  "requestor": "lisa@example.com",
  "target": "john@example.com"
}
```

The API should return the following JSON response on success:

``` json
{
  "success": true
}
```

Please propose JSON responses for any errors that might occur.

**5. As a user, I need an API to block updates from an email address.**

Suppose "andy@example.com" blocks "john@example.com":

- if they are connected as friends, then "andy" will no longer receive notifications from "john"
- if they are not connected as friends, then no new friends connection can be added

The API should receive the following JSON request:

``` json
{
  "requestor": "andy@example.com",
  "target": "john@example.com"
}
```

The API should return the following JSON response on success:

``` json
{
  "success": true
}
```

Please propose JSON responses for any errors that might occur.

**6. As a user, I need an API to retrieve all email addresses that can receive updates from an email address.**

Eligibility for receiving updates from i.e. "john@example.com":
- has not blocked updates from "john@example.com", and
- at least one of the following:
  - has a friend connection with "john@example.com"
  - has subscribed to updates from "john@example.com"
  - has been @mentioned in the update

The API should receive the following JSON request:

``` json
{
  "sender":  "john@example.com",
  "text": "Hello World! kate@example.com"
}
```

The API should return the following JSON response on success:

``` json
{
  "success": true,
  "recipients":
    [
      "lisa@example.com",
      "kate@example.com"
    ]
}
```

Please propose JSON responses for any errors that might occur.

### Constraints

#### Time

7 days from XXXX. Please feel free to submit your work any time, before the deadline.

Please timebox yourself to a maximum of 4 hours for this activity.

#### Technology

You are required to use any of these languages: [Go](https://golang.org/), [Ruby](https://www.ruby-lang.org/en/), JavaScript, Java, PHP or Python.

You are allowed to use any frameworks for the language you chose.

#### Testing

Please approach this exercise as you would in your day-to-day development workflow.

If you write tests in your daily work, we would love to see them in this exercise too.

#### Git and Commit History

Sync your app to GitHub and allow access to `winston` and `miccheng`.

Please maintain a descriptive and clear Git commit history as it would allow us to better understand your thought process.

### Follow Up

In the event that you are selected for the next round of interview (onsite chat),
please be expected to discuss your work further with us during the interview.

Looking forward to seeing your code!

----

## Frontend Engineer

### Background

In Singapore Power, we are often required to build internal or external dashboards
to communicate critical and relevant information to our stakeholders and consumers.

The latest myENV iOS app is an example of a dashboard that we might build:
A dashboard with critical information and relevant interactive visualisations on the Home screen.

### Your Task

Using myENV iOS app as the reference, please implement the Home screen in HTML5 (and JS and CSS).

You only need to implement the layout of the Home screen with appropriate assets,
and pick a visualisation for the interactive Tide/Sunrise & Sunset chart.

![img_5577](../img/take_home_test_monile_screenshot.png)

Please ensure that the HTML5 Home screen works in Safari (Desktop and iOS).

Please write sufficient documentation to help us run an instance of your HTML5 Home screen.

### Constraints

#### Time

7 days from XXXX. Please feel free to submit your work any time, before the deadline.

Please timebox yourself to a maximum of 8 hours for this activity.

#### Technology

You are free to use any JavaScript framework or library. ReactJS is preferred.

#### Testing

Please approach this exercise as you would in your day-to-day development workflow.

If you write tests in your daily work, we would love to see them in this exercise too.

#### Git and Commit History

Sync your app to GitHub and allow access to `winston` and `miccheng`.

Please maintain a descriptive and clear Git commit history as it would allow us to better understand your thought process.

### Follow Up

In the event that you are selected for the next round of interview (onsite chat),
please be expected to discuss your work further with us during the interview.

Looking forward to seeing your code!

----

## iOS Engineer

### Background

In Singapore Power, we are often required to build iOS apps backed by internal data APIs
to communicate critical and relevant information to our stakeholders and consumers.

### Your Task

Build an iOS app to display the PSI index of (East, West, North, South and Central) Singapore.

Please use the data from https://data.gov.sg/dataset/psi and display it on a Map.

We will evaluate this exercise based on:

- Completion of the application
- User Interface
- Code Structure
- Tests

You may use the following Testing frameworks:

- [Quick](https://github.com/Quick/Quick)
- [Nimble](https://github.com/Quick/Nimble)

### Constraints

#### Time

7 days from XXXX. Please feel free to submit your work any time, before the deadline.

Please timebox yourself to a maximum of 8 hours for this activity.

#### Technology

You are required to use Swift.

#### Testing

Please approach this exercise as you would in your day-to-day development workflow.

If you write tests in your daily work, we would love to see them in this exercise too.

#### Git and Commit History

Sync your app to GitHub and allow access to `subhransu` and `ivanfoong`.

Please maintain a descriptive and clear Git commit history as it would allow us to better understand your thought process.

### Follow Up

In the event that you are selected for the next round of interview (onsite chat),
please be expected to discuss your work further with us during the interview.

Looking forward to seeing your code!

----

## Android Engineer

### Background

In Singapore Power, we are often required to build Android apps backed by internal data APIs
to communicate critical and relevant information to our stakeholders and consumers.

### Your Task

Build an Android app to display the PSI index of (East, West, North, South and Central) Singapore.

Please use the data from https://data.gov.sg/dataset/psi and display it on a Map.

We will evaluate this exercise based on:

- Completion of the application
- User Interface
- Code Structure
- Tests

You may use JUnit or any other Testing framework of your choice to write the unit tests.

### Constraints

#### Time

7 days from XXXX. Please feel free to submit your work any time, before the deadline.

Please timebox yourself to a maximum of 8 hours for this activity.

#### Technology

You are required to use Java and native Android APIs.

#### Testing

Please approach this exercise as you would in your day-to-day development workflow.

If you write tests in your daily work, we would love to see them in this exercise too.

#### Git and Commit History

Sync your app to GitHub and allow access to `subhransu` and `ivanfoong`.

Please maintain a descriptive and clear Git commit history as it would allow us to better understand your thought process.

### Follow Up

In the event that you are selected for the next round of interview (onsite chat),
please be expected to discuss your work further with us during the interview.

Looking forward to seeing your code!

----

## IoT Engineer

### Test 1: Real-time sensor data extraction

> This take home test is given typically to a fresh graduate.

#### Objective

To verify that the candidate has basic knowledge of implementing IoT solutions with the following technologies:

1. Environmental sensors and actuators
1. Hardware modules
1. Linux operating system
1. Wireless protocols
1. Backend programming
1. Data processing

#### Background

Singapore Power Digital Team is looking for an IoT engineer to collaborate on managing many sensor nodes interfacing with various energy infrastructure. The team understands fully that IoT is a new technology and not all answers are known. Hence, we intend to engineer solutions together as a team while asking the right questions, knowing the limitations, assumptions and challenges in each project.

#### Your Task

Build a simple Internet of Things system with your choice of an environmental sensor that is connected to a single board computer (E.g. Latest Raspberry Pi with WiFi) running a Linux operating system . Read the data from the sensor real-time and display it in your laptop browser with a local web server. The entire setup should work without any connection to the Internet.

Please write to us and explain the following questions below:

1. Which environmental sensor and single board computer did you use? Why?
1. How did you get the data out from the sensor to your laptop?
1. How did you display the data trend in your laptop browser that makes a meaningful sense?
1. What could be some challenges if you want to deploy 100 of the same setup?
1. Elaborate on all assumptions, limitations and challenges you faced while building this.
1. Include some photos and/or media of your setup.
1. Upload your source code to GitHub and provide the URL. Include any major references that you have used for building this.

#### Duration

7 days from XXXX. Please feel free to submit your work any time, before the deadline.

Please timebox yourself to a maximum of `8 hours` for this activity.

#### References

You can access any material such as Internet search or books for research and reference to answer the questions.

### Test 2: IoT Software focus

> This take home test is given typically to an experienced candidate with a software focus.

#### Objective

To verify that the candidate has effective knowledge of implementing IoT solutions with the following software technologies:

1. Linux operating system
1. Computer security
1. Device authentication
1. Wireless and application layer protocols
1. Computer networking

#### Background

Singapore Power Digital Team is looking for an IoT engineer to collaborate on managing many sensor nodes interfacing with various energy infrastructure. The team understands fully that IoT is a new technology and not all answers are known. Hence, we intend to engineer solutions together as a team while asking the right questions, knowing the limitations, assumptions and challenges in each project.

#### Your Task

Imagine, in the near future Singapore Power (SP) has 2000 solar panels installed on the roof tops of neighbourhood community centers distributed all around Singapore. Using single board computers (SBC) running Linux operating system (E.g. Beaglebone, RaspberryPI), the solar panel data is extracted every second and transmitted to the cloud wirelessly in a secure manner.

Design and code a backend program called "home" that will reside in the cloud to uniquely identify each of the 2000 SBC when they are switched on. The program should register the SBC's unique ID and the time when it first makes contact with "home" upon waking up. It should also make contact with the "home" program at regular intervals to indicate that it is still alive.

Please write to us and explain the following questions below:

1. From your code, what security measures should be put in place in each SBC and networking to ensure security?
1. From your code, explain the device authentication and provisioning workflow to ensure that 2000 SBCs are registered in the "home" program in a secure and efficient manner.
1. From your code, what protocol did the SBC use to communicate with the program "home"?
1. In future, imagine there is a Linux security patch. How will you remotely update each SBC securely using "home"?
1. What is the payload as part of the regular contact with "home"? What interval did you choose? Why?
1. How do you think the solar panel is connected to the SBC if we collect data such as solar irradiance and current?
1. Elaborate on all assumptions, limitations and challenges you faced while building this.
1. Upload your source code to GitHub and provide the URL. Include any major references that you have used for building this.

#### Duration

7 days from XXXX. Please feel free to submit your work any time, before the deadline.

Please timebox yourself to a maximum of `8 hours` for this activity.

#### References

You can access any material such as Internet search or books for research and reference to answer the questions.

### Test 3: IoT Hardware focus

> This take home test is given typically to an experienced candidate with a hardware focus.

#### Objective

To verify that the candidate has effective knowledge of implementing IoT solutions with the following hardware technologies:

1. Electronics schematic
1. PCB layout
1. Electronics components
1. Power management
1. Wireless technologies
1. Electronics manufacturing
1. Firmware development

#### Background

Singapore Power Digital Team is looking for an IoT engineer to collaborate on managing many sensor nodes interfacing with various energy infrastructure. The team understands fully that IoT is a new technology and not all answers are known. Hence, we intend to engineer solutions together as a team while asking the right questions, knowing the limitations, assumptions and challenges in each project.

#### Your Task

Imagine, the IoT team wants to measure total power consumption of a single community centre in real-time. The data from this power monitoring device has to be sent back to a central cloud server via Internet.

Design a simple schematic for a power monitoring device that is able to measure the total building load in real-time using a CT sensor. You can use an electronics design software of your choice or use [KiCad](https://kicad-pcb.org/), a free and open source electronics design software.

Add the firmware code that extracts the required values and transfers them to the cloud. The data from the CT sensor should be transmitted wirelessly to a central cloud infrastructure. You can refer to https://learn.openenergymonitor.org/electricity-monitoring/ct-sensors/introduction for designing a wireless CT sensor.

Please write to us and explain the following questions below:

1. What are some general challenges in designing an IoT system with embedded electronics and wireless capabilities?
1. From your designed schematic, explain the electrical components, circuitry, power management and wireless connectivity in designing this power monitoring device. You can also give specific part number of electronics components that you choose and the assumptions/limitations for using them.
1. From your firmware code, explain the protocol you used to communicate the voltage and current values to the central cloud infrastructure.
1. The IoT team now wants to roll out the single prototype built in the previous questions to 1000 buildings. Explain the process of the manufacturing, assembling and deploying it to these buildings.
1. Briefly explain the data flow in the cloud that processes the values received from the CT sensor. Describe how the data might be received, processed and stored. What specific technologies are suitable for these tasks?
1. Elaborate on all assumptions, limitations and challenges you faced while building this.
1. Which electronics design software did you use to create the schematic? Upload your schematic (in JPG/PNG/PDF format), any other electronics design files and source code to GitHub and provide the URL. Include any major references that you have used for building this.

#### Duration

7 days from XXXX. Please feel free to submit your work any time, before the deadline.

Please timebox yourself to a maximum of `8 hours` for this activity.

#### References

You can access any material such as Internet search or books for research and reference to answer the questions.
